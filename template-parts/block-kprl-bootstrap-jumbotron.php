<?php
/**
 * Block Name: kprl Bootstrap Jumbotron
 *
 * This is the template that displays a jumbotron.
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  // create id attribute for specific styling
  $id = 'jumbotron-' . $block['id'];

  // create align class ("alignwide") from block setting ("wide")
  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  $splitcontent = false;

  ?>
  <div id="<?php echo $id; ?>" class="jumbotron row <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php if ( array_key_exists( 'jumbotron_image', $fields ) ): ?>
      <?php if ( $fields['jumbotron_image'] ): ?>
        <?php $splitcontent = true; ?>
        <div class="col-12 col-sm-3 text-center">
          <img src="<?php echo $fields['jumbotron_image']['sizes']['medium']; ?>"/>
        </div>
      <?php endif; ?>
    <?php endif; ?>

    <div class="col row no-gutters">

      <div class="col-12">
        <?php if ( $fields['jumbotron_title'] ): ?>
          <h4><?php echo $fields['jumbotron_title']; ?></h4>
        <?php endif; ?>
      </div>

      <div class="col-12 <?php if(!$splitcontent){ echo 'col-md-8'; } ?>">
        <p><?php echo $fields['jumbotron_text']; ?></p>
      </div>

      <div class="col-12 wp-block-button text-right <?php if(!$splitcontent){ echo 'col-md-4'; } ?>">
        <a class="" target="<?php echo $fields['jumbotron_link_target']; ?>" href="<?php echo $fields['jumbotron_link']; ?>"><?php echo $fields['jumbotron_button_text']; ?> <i class="fas fa-angle-right"></i></a>
      </div>

      <!--
      <a class="btn btn-primary btn-lg" href="<?php echo $fields['jumbotron_link']; ?>" role="button"><?php echo $fields['jumbotron_button_text']; ?></a>
      <a class="wp-block-button__link" href="<?php echo $fields['jumbotron_link']; ?>"><?php echo $fields['jumbotron_button_text']; ?></a>
      -->

    </div>

  </div>

  <?php

endif;
