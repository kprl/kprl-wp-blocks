<div class="card-deck no-gutters">

<?php
foreach ($staffingArr as $key => $staff):
  $ubidkey = $block['id'] . "-" . $key;
?>

  <div class="card">
    <img src="<?php echo get_the_post_thumbnail_url($staff['ID'], 'medium'); ?>" class="card-img-top" alt="<?php echo $staff['title']; ?>">
    <div class="card-body">
      <h5 class="card-title"><?php echo $staff['title']; ?></h5>
      <div class="card-text">
        <div class="kprl-staffing-contact">
          <?php
          if ( is_array($staff['contact']) ):

            foreach ($staff['contact'] as $key => $contact) {
              if ( $contact['key'] == 'email' OR $contact['key'] == 'phone' ) {
                ?>
                <p>
                  <a href="<?php echo $contact['value']['link']; ?>" target="<?php echo $contact['value']['target']; ?>" class="<?php echo $contact['class']; ?>"><?php echo $contact['value']['display']; ?></a>
                </p>
                <?php
              }
            }

          endif;
          ?>
        </div>
        <?php if ( $staff['content'] ): ?>
          <div class="kprl-staffing-content">
            <?php echo $staff['content']; ?>
          </div>
        <?php endif; ?>
        <div class="kprl-staffing-departments">
          <?php
          if ( is_array($staff['departments']) ):
            foreach ($staff['departments'] as $tkey => $ds) {
              $ant = count($staff['departments']);
              if ( $ant == $tkey + 2 ) { $comma = " och "; } else if ( $ant > $tkey + 1 ) { $comma = ", "; } else { $comma = null; }
              echo "<span>" . $ds['title'] . $comma . "</span>";
            }
          endif;
          ?>
        </div>
      </div>
    </div>
  </div>

<?php endforeach; ?>

</div>
