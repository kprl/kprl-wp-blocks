<?php
  if ( $align_class == "alignwide" ) {
    $nogutters = null;
  } else if ( $align_class == "alignfull" ) {
    $nogutters = null;
  } else {
    $nogutters = " no-gutters";
  }
?>

<div class="row <?php echo $nogutters; ?>">

<?php
foreach ($staffingArr as $key => $staff):
  $ubidkey = $block['id'] . "-" . $key;

  if ( $align_class == "alignwide" ) {
    $classes = "col col-md-4";
  } else if ( $align_class == "alignfull" ) {
    $classes = "col col-md-3";
  } else {
    $classes = "col-12";
  }
?>

  <div class="<?php echo $classes; ?>">

    <div class="kprl-staff">
      <main>
        <section>
          <div class="media">
            <img src="<?php echo get_the_post_thumbnail_url($staff['ID'], 'thumbnail'); ?>" class="align-self-center rounded-circle" alt="<?php echo $staff['title']; ?>">
            <div class="media-body">
              <h5><?php echo $staff['title']; ?></h5>
              <div class="kprl-staffing-departments">
                <?php
                if ( is_array($staff['departments']) ):
                  foreach ($staff['departments'] as $tkey => $ds) {
                    $ant = count($staff['departments']);
                    if ( $ant == $tkey + 2 ) { $comma = " och "; } else if ( $ant > $tkey + 1 ) { $comma = ", "; } else { $comma = null; }
                    echo "<span>" . $ds['title'] . $comma . "</span>";
                  }
                endif;
                ?>
              </div>
            </div>

            <?php
            if ( is_array($staff['contact']) ):
            ?>
            <button id="button-<?php echo $ubidkey;?>" value='<?php echo $ubidkey;?>' onclick="kprlExpandStaffContact(this)"/>
              <span>Kontakta</span>

              <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"> <g class="nc-icon-wrapper" fill="#444444"> <path d="M14.83 30.83L24 21.66l9.17 9.17L36 28 24 16 12 28z"></path> </g> </svg>
            </button>
            <?php
            endif;
            ?>
          </div>

          <div id="title-<?php echo $ubidkey;?>" class="title"><p>Kontakta</p></div>
        </section>

      </main>

      <nav id="nav-<?php echo $ubidkey;?>">

        <?php
        if ( is_array($staff['contact']) ):

          foreach ($staff['contact'] as $key => $contact) {

            ?>

            <a href="<?php echo $contact['value']['link']; ?>" target="<?php echo $contact['value']['target']; ?>" class="<?php echo $contact['class']; ?>">
              <div class="icon">
                <?php echo $contact['svg']; ?>
              </div>

              <div class="content">
                <h1><?php echo $contact['title']; ?></h1>
                <span><?php echo $contact['value']['display']; ?></span>
              </div>

              <svg class="arrow" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48"> <g class="nc-icon-wrapper" fill="#444444"> <path d="M17.17 32.92l9.17-9.17-9.17-9.17L20 11.75l12 12-12 12z"></path> </g> </svg>
            </a>

            <?php

          }

        endif;
        ?>

      </nav>
    </div>

  </div>

<?php endforeach; ?>

</div>
