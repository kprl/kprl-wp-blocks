<div class="card-deck no-gutters">

<?php
foreach ($staffingArr as $key => $staff):
  $ubidkey = $block['id'] . "-" . $key;

  if ( $align_class == "alignwide" ) {
    $classes = "col col-md-4";
  } else if ( $align_class == "alignfull" ) {
    $classes = "col col-md-3";
  } else {
    $classes = "col-12 col-md-6";
  }
?>

  <div class="<?php echo $classes; ?>">
    <div class="card mb-3" >
      <div class="row no-gutters">
        <div class="col-4 col-xl-4 align-self-center">
          <img src="<?php echo get_the_post_thumbnail_url($staff['ID'], 'medium'); ?>" class="card-img-top" alt="<?php echo $staff['title']; ?>">
        </div>
        <div class="col-8 col-xl-8">
          <div class="card-body">
            <h5 class="card-title"><?php echo $staff['title']; ?></h5>
            <div class="card-text">
              <div class="kprl-staffing-contact">
                <?php
                if ( is_array($staff['contact']) ):

                  foreach ($staff['contact'] as $key => $contact) {
                    if ( $contact['key'] == 'email' OR $contact['key'] == 'phone' ) {
                      ?>
                      <p>
                        <a href="<?php echo $contact['value']['link']; ?>" target="<?php echo $contact['value']['target']; ?>" class="<?php echo $contact['class']; ?>"><?php echo $contact['value']['display']; ?></a>
                      </p>
                      <?php
                    }
                  }

                endif;
                ?>
              </div>
              <div class="kprl-staffing-departments">
                <?php
                if ( is_array($staff['departments']) ):
                  foreach ($staff['departments'] as $tkey => $ds) {
                    $ant = count($staff['departments']);
                    if ( $ant == $tkey + 2 ) { $comma = " och "; } else if ( $ant > $tkey + 1 ) { $comma = ", "; } else { $comma = null; }
                    echo "<span>" . $ds['title'] . $comma . "</span>";
                  }
                endif;
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endforeach; ?>

</div>
