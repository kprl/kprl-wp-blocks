<?php
/**
 * Block Name: kprl Image Gallery
 *
 *
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $id = 'kprl-image-gallery-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  $strBase    = "block_kprl_gallery_";
  $strTitle   = $fields[$strBase  . "title"];
  $strGallery = $fields[$strBase  . "gallery"];
  $strStyle   = $fields[$strBase  . "style"];

  if ( $strStyle == 'gallery' ) {
    $strColumns = $fields[$strBase  . "columns"];
  }

  $antImages  = count($strGallery);

  ?>

  <div id="<?php echo $id; ?>" class="kprl-image-gallery <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( $fields['block_kprl_gallery_title'] ) {
        echo "<h3>" . $strTitle . "</h3>";
      }
    ?>

    <?php
      if ( is_admin() ):
        echo '<ul class="wp-block-gallery alignwide columns-' . $strColumns . ' is-cropped">';
        foreach ($strGallery as $key => $img) {
          echo '<li class="blocks-gallery-item">';
            echo '<figure class="" tabindex="-1">';
              echo "<img src='" . $img['sizes']['thumbnail'] . "' alt='' data-id='" . $img['ID'] . "' tabindex='0' aria-label='image " . $key . " of " . $antImages . " in gallery' />";
            echo '</figure>';
          echo '</li>';
        }
        echo '</ul>';
      elseif ( $strStyle == 'slideshow' ):
        echo "Kommer inom kort!";
      elseif ( $strStyle == 'gallery' ):
    ?>

    <ul class="wp-block-gallery alignwide columns-<?php echo $strColumns; ?> is-cropped">
      <?php
        $i = 0;
        foreach ($strGallery as $key => $img) {
          $attr = array(
            'class' => '',
            'data-id' => $img['ID'],
            'tabindex' => 0,
            'aria-label' => 'Bild ' . $key . ' av ' . $antImages . ' i detta bildgalleri.',
          );
          $imgsrc = $img['url'];

          echo '<li class="blocks-gallery-item">';
            echo '<a class="d-block" href="' . $imgsrc . '">';
              echo '<figure class="" tabindex="-1">';
                echo wp_get_attachment_image( $img['ID'], 'medium', false, $attr );
              echo '</figure>';
            echo '</a>';
          echo '</li>';

        }
      ?>
    </ul>

    <?php
      endif;
    ?>

  </div>

  <?php

endif;
