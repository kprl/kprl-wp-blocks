<?php
/**
 * Block Name: kprl Bemanning
 *
 *
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $id = 'kprl-staffing-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  if ( $fields['kprl-staffing-content'] == "all-staffing" OR $fields['kprl-staffing-content'] == "all-departments" ) {
    $staffingArr = return_staffing_array( $fields['kprl-staffing-content'] );
  } else if ( $fields['kprl-staffing-content'] == "specific-staffing" ) {
    $staffingArr = return_staffing_array( $fields['kprl-staffing-content'], $fields['kprl-staffing-specific-staffing'] );
  } else if ( $fields['kprl-staffing-content'] == "specific-departments" ) {
    $staffingArr = return_staffing_array( $fields['kprl-staffing-content'], $fields['kprl-staffing-specific-departments'] );
  }

  $layout_folder = KWPB_PLUGIN_TEMPLATE . '/kprl-staffing-layouts/';

?>

  <div id="<?php echo $id; ?>" class="kprl-staffing container-fluid <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( is_admin() ):
        include $layout_folder . 'admin.php';
      else:

        // plain
        // card-horizontal
        // card-group
        // card-deck
        // card-columns
        // contact-expand

        include $layout_folder . $fields['kprl-staffing-layout'] . '.php';

      endif;
    ?>

  </div>

  <?php

endif;
