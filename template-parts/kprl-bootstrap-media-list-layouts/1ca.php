<div class="col-12 order-md-0 media-big">
  <?php
    $link = esc_url( get_permalink($posts_array[0]->ID) );
    if ( function_exists('display_kwpb_media_thumb') ) {
      display_kwpb_media_thumb( $posts_array[0]->ID, $posttype, $link );
    }
  ?>
  <div class="media-body">
    <h3 class="media-title"><a href="<?php echo $link; ?>"><?php echo $posts_array[0]->post_title; ?></a></h3>
    <div class="row no-gutters media-time-more"><!-- justify-content-between -->
      <div class="timediff col-auto"><i class="far fa-clock"></i><span><?php echo return_kprl_human_time_string( $posts_array[0]->post_date, 2630880 * 6 ); ?></span></div>
      <span class="col-auto">&nbsp;–&nbsp;</span>
      <div class="col-auto"><a class="readmore" href="<?php echo $link; ?>">Läs mer</a></div>
    </div>
    <?php
      if ( $fields['display_text'] == 'excerpt' ) {
        if ( empty( $posts_array[0]->post_excerpt ) ) {
          $excerpt = wp_kses_post( wp_trim_words( $posts_array[0]->post_content, 25, ' ...' ) );
        } else {
          $excerpt = wp_kses_post( $posts_array[0]->post_excerpt );
        }
        echo apply_filters('the_excerpt', $excerpt);
      } elseif ( $fields['display_text'] == 'content' ) {
        echo wp_kses_post( $posts_array[0]->post_content );
      } else {
        echo '';
      }
    ?>
  </div>
</div>

<?php unset( $posts_array[0] ); ?>

<div class="col-12 order-md-2">

  <ul style="background-color: <?php echo hex2rgba($term_color, 0.15); ?>;">

  <?php
    foreach ($posts_array as $key => $post):
      $link = esc_url( get_permalink($post->ID) );
  ?>

    <li class="media">
      <a href="<?php echo $link; ?>"><?php echo get_the_post_thumbnail( $post->ID, array(60, 60) ); ?></a>
      <div class="media-body">
        <h4 class="media-title"><a href="<?php echo $link; ?>"><?php echo $post->post_title; ?></a></h4>
        <div class="row no-gutters">
          <div class="timediff col-auto"><i class="far fa-clock"></i><span><?php echo return_kprl_human_time_string( $post->post_date, 2630880 * 6 ); ?></span></div>
          <span class="col-auto">&nbsp;–&nbsp;</span>
          <div class="col-auto"><a class="readmore" href="<?php echo $link; ?>">Läs mer</a></div>
        </div>
      </div>
    </li>

  <?php
    endforeach;
  ?>

  </ul>
</div>
