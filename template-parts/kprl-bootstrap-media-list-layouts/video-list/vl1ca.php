<div class="col-12 order-md-0 media-big">
  <!-- wp-block-video -->
  <figure class="">
    <div id="kwpb-video-title-<?php echo $block['id']; ?>" class="kwpb-video-title badge badge-light text-wrap"><?php echo $posts_array[0]->post_title; ?></div>
    <video id="kwpb-video-changer-<?php echo $block['id']; ?>" class="kwpb-video-changer" controls="" src="<?php echo esc_url( get_the_guid($posts_array[0]->ID) ); ?>" data-origwidth="0" data-origheight="0"></video>
  </figure>
</div>

<div class="col-12 order-md-2">

  <?php
    if ( !$fields['display_max_list_height'] OR $fields['display_max_list_height'] == 0 ) {
      $ulstylemaxheight = "max-height: 318px; overflow-y: auto;";
    } else {
      $ulstylemaxheight = "";
    }
  ?>

  <ul class="kwpb-video-list" style="background-color: <?php echo hex2rgba($term_color, 0.15); ?>; <?php echo $ulstylemaxheight; ?>">

  <?php
    foreach ($posts_array as $key => $post):
      $link = esc_url( get_the_guid($post->ID) );
  ?>

    <li value="<?php echo $link; ?>" title="<?php echo $post->post_title; ?>" blockid="<?php echo $block['id']; ?>" onclick="kwpbchangevideo(this)" id="kwpb-video-<?php echo $block['id'] . "-" . $key; ?>" class="btn-link media position-relative">
      <div class="media-body">
        <h4 class="media-title col-auto float-left"><?php echo $post->post_title; ?></h4>
        <?php
          $terms = wp_get_object_terms($post->ID, 'mediamatic_wpfolder');
          foreach ($terms as $t) {
            echo '<span class="badge badge-pill badge-info">' . get_term( $t->term_id )->name . '</span>';
          }
        ?>
        <div class="timediff float-right"><i class="far fa-clock"></i><span><?php echo return_kprl_human_time_string( $post->post_date, 2630880 * 6 ); ?></span></div>
        <?php echo wpautop(wp_get_attachment_caption($post->ID)); ?>
      </div>
    </li>

  <?php
    endforeach;
  ?>

  </ul>
</div>
<script type="text/javascript">
  function kwpbchangevideo(item) {
    var bId = $(item).attr('blockid');
    var tId = $(item).attr("id");
    var badgeId = "kwpb-video-title-" + bId;
    var videoId = "#" + "kwpb-video-changer-" + bId;

    var addElem = document.getElementById(tId);
    jQuery(item).ready(function($){
      document.getElementById(badgeId).innerHTML = $(item).attr("title");
      $(videoId).attr('src',$(item).attr("value"));
      $(videoId).attr('autoplay','true');

      $(".bg-transparent").attr("class","btn-link media position-relative");
      addElem.classList.add("bg-transparent");
    });
  }
</script>
