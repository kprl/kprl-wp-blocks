<div class="card-columns">

  <?php
    foreach ($posts_array as $key => $post):
      $link = esc_url( get_the_guid($post->ID) );
  ?>

    <div class="card">
      <figure class="card-img-top"><video controls="" src="<?php echo $link; ?>" data-origwidth="0" data-origheight="0"></video></figure>
      <div class="card-body">
        <h5 class="card-title"><?php echo $post->post_title; ?></h5>
        <p class="card-text"><?php echo nl2br(wp_get_attachment_caption($post->ID)); ?></p>
      </div>
      <div class="card-footer">
        <small class="timediff"><i class="far fa-clock"></i><span><?php echo return_kprl_human_time_string( $post->post_date, 2630880 * 6 ); ?></span></small>
      </div>
    </div>

  <?php
    endforeach;
  ?>

</div>
