<?php
/**
 * Block Name: kprl Bootstrap Carousel
 *
 * This is the template that displays a carousel block.
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  // create id attribute for specific styling
  $id = 'carousel-' . $block['id'];

  // create align class ("alignwide") from block setting ("wide")
  $align_class  = $block['align'] ? 'align' . $block['align'] : '';

  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  if ( $fields['style'] == 'fade' ) {
    $style = 'carousel-fade';
  } else if ( $fields['style'] == 'slide' ) {
    $style = 'carousel-slide';
  } else {
    $style = '';
  }

  ?>
  <div id="<?php echo $id; ?>" class="carousel slide <?php echo $style; ?> <?php echo $align_class; ?> <?php echo $css_class; ?>" data-ride="carousel">
  <?php if ( $fields['indicators'] ) { ?>
    <ol class="carousel-indicators">
      <?php
      $a = 0;
      while ($a < count($fields['gallery'])) {
        if ( $a == 0 ) { $class = "active"; } else { $class = ""; }
        ?>
        <li data-target="#<?php echo $id; ?>" data-slide-to="<?php echo $a; ?>" class="<?php echo $class; ?>"></li>
        <?php
        $a++;
      }
      ?>
    </ol>
  <?php } ?>
  <div class="carousel-inner">
  <?php

  $b = 0;
  foreach ($fields['gallery'] as $key => $image) {
    //$bild   = $image['url']; //ORIGINAL
    //$bredd  = $image['width']; //ORIGINAL
    $bild   = $image['sizes']['large'];
    $bredd  = $image['sizes']['large-width'];
    $rubbe  = $image['title'];
    if ( isset($image['caption']) ) {
      $text = $image['caption'];
    }

    $halv_bredd   = $bredd / 2;
    $center_style = "left: 50%; margin-left: -" . $halv_bredd . "px;";

    if ( $b == 0 ) { $class = "active"; } else { $class = ""; }
    $b++;

    ?>

    <div class="carousel-item <?php echo $class; ?>">
      <img src="<?php echo $bild; ?>" alt="<?php echo $rubbe; ?>">
      <div class="carousel-caption d-none d-md-block">
        <?php
          if ( $fields['image_title'] ) {
            echo "<h5>" . $rubbe . "</h5>";
          }
          if ( $fields['image_text'] ) {
            echo "<p>" . $text . "</p>";
          }
        ?>
      </div>
    </div>

    <?php
  }

  ?>
  </div>
    <?php if ( $fields['controls'] ) { ?>
      <a class="carousel-control-prev" href="#<?php echo $id; ?>" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only"><?php __( 'Previous', 'twentynineteen' ); ?></span>
      </a>
      <a class="carousel-control-next" href="#<?php echo $id; ?>" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only"><?php __( 'Next', 'twentynineteen' ); ?></span>
      </a>
    <?php } ?>
  </div>

  <?php

endif;
