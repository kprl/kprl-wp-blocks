<?php
/**
 * Block Name: kprl Bootstrap Media list
 *
 * This is the template that displays a media list.
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $layout_folder  = KWPB_PLUGIN_TEMPLATE . '/kprl-bootstrap-media-list-layouts/';
  $layout_default = $layout_folder . '2ca.php';
  $layout_file    = $layout_default;
  $term_color     = '#009FE3';
  $term_name      = "Titel";

  $quantity = 0;
  if ( $fields['display_quantity'] ) {
    $quantity = $fields['display_quantity'];
  }
  if ( $quantity == 0 ) {
    $quantity = -1;
  }

  if ( $fields['display_content'] == 'posttype' ) {

    $posttype = $fields['medialist_posttype']['posttype'];
    $mq = '';

    if ( $posttype == 'post' ) {
      $term_name = 'Inlägg';
    } else if ( $posttype == 'glimt_livestream' ) {
      $term_name = 'Livestream';
      $mq = array(
        array(
          'key'         => 'glimt_livestream_datetime_date',
          'value'       => date('Ymd', strtotime("now")),
          'compare'     => '<='
        )
      );
    }

    $args = array(
      'posts_per_page'    => $quantity,
      'offset'            => 0,
      'post_type' 		    => $posttype,
      'orderby'           => 'post_date',
      'order'             => 'DESC',
      'post_status'       => 'publish',
      'suppress_filters'  => true,
      'meta_query'        => $mq,
    );
    $posts_array = get_posts( $args );

  } else if ( $fields['display_content'] == 'category' ) {

    $posttype = 'posts';

    $tid = $fields['medialist_category']['categories'];
    $term_child = array();
    $term_name = "";

    if ( is_array($tid) ) {
      if ( sizeof($tid) == 1 ) {
        $term_name = get_the_category_by_ID( $tid[0] );
        $term_meta = get_term_meta( $tid[0] );
      } else {
        $term_name = "Utvalda kategorier";
        $term_meta = array();
        foreach ($tid as $ttt) {
          $term_child = get_term_children( $ttt, 'category' );
        }
      }
    } else {
      $term_name = "Alla kategorier";
    }

    if ( isset($term_meta['kwpb-taxonomy-color']) ) {
      $term_color = $term_meta['kwpb-taxonomy-color'][0];
    }

    $args = array(
      'posts_per_page'   => $quantity,
      'offset'           => 0,
      'category'         => $tid,
      'category__not_in' => $term_child,
      'orderby'          => 'date',
      'order'            => 'DESC',
      'post_type'        => 'post',
      'post_status'      => 'publish',
      'suppress_filters' => true,
    );
    $posts_array = get_posts( $args );

  } else if ( $fields['display_content'] == 'video' ) {

    $posttype = 'attachment';

    $tid = $fields['medialist_videolist']['folders'];

    $term_child = array();
    $term_name = "";

    if ( is_array($tid) ) {
      if ( sizeof($tid) == 1 ) {
        $term_name = get_the_category_by_ID( $tid[0] );
        $term_meta = get_term_meta( $tid[0] );
      } else {
        $term_name = "Utvalda mappar";
        $term_meta = array();
        foreach ($tid as $ttt) {
          $term_child = get_term_children( $ttt, 'mediamatic_wpfolder' );
        }
      }
    } else {
      $term_name = "Alla mappar";
    }

    if ( isset($term_meta['kwpb-taxonomy-color']) ) {
      $term_color = $term_meta['kwpb-taxonomy-color'][0];
    }

    //array('image/png', 'image/jpeg', 'video/mp4', 'application/pdf');

    $args = array(
      'posts_per_page'   => $quantity,
      'offset'           => 0,
      'tax_query'        => array(
        array(
          'taxonomy'     => 'mediamatic_wpfolder',
          'field'        => 'term_id',
          'terms'        => $tid
        )
      ),
      'post_type'        => $posttype,
      'post_status'      => 'inherit',
      'post_mime_type'   => 'video/mp4',
      'suppress_filters' => true,
    );
    $posts_array = get_posts( $args );

  }

  if ( $fields['display_header'] ) {
    if ( $fields['customized']['customized'] ) {
      if ( isset($fields['customized']['customized_color']) AND $fields['customized']['customized_color'] !== "" ) {
        $term_color = $fields['customized']['customized_color'];
      }
      if ( isset($fields['customized']['customized_title']) AND $fields['customized']['customized_title'] !== "" ) {
        $term_name = $fields['customized']['customized_title'];
      }
    }
  }

  // create id attribute for specific styling
  $id = 'medialist-' . $block['id'];

  // create align class ("alignwide") from block setting ("wide")
  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  if ( is_admin() ):
    $layout_file    = $layout_folder . 'admin.php';
    $layout_header  = '';
  else:
    $layout_file    = $layout_folder . 'video-list/' . $fields['display_layout_videolist'] . '.php';
    $layout_header  = '<div style="background: ' . $term_color . '; border-bottom-color: ' . $term_color .';" class="term-border col-12">
      <h2>' . $term_name . '</h2>
      <div class="col-auto ml-auto"></div>
    </div>';
  endif;

  ?>

  <div id="<?php echo $id; ?>" class="medialist row no-gutters shadow-sm <?php echo $align_class; ?> <?php echo $css_class; ?>" style="border-color: <?php echo $term_color; ?>;">



    <?php
      if ( $fields['display_header'] ) {
        echo $layout_header;
      }
      if ( file_exists($layout_file) ) {
        include $layout_file;
      } else {
        include $layout_default;
      }
    ?>

  </div>

  <?php

endif;
