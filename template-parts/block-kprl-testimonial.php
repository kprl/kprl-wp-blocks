<?php
/**
 * Block Name: kprl Bemanning
 *
 *
 */

// get image field (array)
$fields = get_fields();

if( $fields ):

  $id = 'kprl-testimonial-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if (in_array('className', $block)) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }

  if ( $fields['kprl-testimonial-content'] == "all-testimonial" OR $fields['kprl-testimonial-content'] == "all-departments" ) {
    $testimonialArr = return_testimonial_array( $fields['kprl-testimonial-content'] );
  } else if ( $fields['kprl-testimonial-content'] == "specific-testimonial" ) {
    $testimonialArr = return_testimonial_array( $fields['kprl-testimonial-content'], $fields['kprl-testimonial-specific-testimonial'] );
  } else if ( $fields['kprl-testimonial-content'] == "specific-departments" ) {
    $testimonialArr = return_testimonial_array( $fields['kprl-testimonial-content'], $fields['kprl-testimonial-specific-departments'] );
  }

  $layout_folder = KWPB_PLUGIN_TEMPLATE . '/kprl-testimonial-layouts/';

?>

  <div id="<?php echo $id; ?>" class="kprl-testimonial container-fluid <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( is_admin() ):
        include $layout_folder . 'admin.php';
      else:

        include $layout_folder . $fields['kprl-testimonial-layout'] . '.php';

      endif;
    ?>

  </div>

  <?php

endif;
