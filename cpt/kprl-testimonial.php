<?php

if ( ! function_exists('kprl_testimonial_func') ) {

  // Register Custom Post Type
  function kprl_testimonial_func() {

  	$labels = array(
      'name'                  => _x( 'Rekommendationer', 'Post Type General Name', 'kwpb_td' ),
  		'singular_name'         => _x( 'Rekommendation', 'Post Type Singular Name', 'kwpb_td' ),
  		'menu_name'             => __( 'Rekommendationer', 'kwpb_td' ),
  		'name_admin_bar'        => __( 'Rekommendationer', 'kwpb_td' ),
  		'archives'              => __( 'Rekommendation-arkiv', 'kwpb_td' ),
  		'attributes'            => __( 'Rekommendation-attribut', 'kwpb_td' ),
  		'parent_item_colon'     => __( 'Rekommendation-förälder:', 'kwpb_td' ),
  		'all_items'             => __( 'Alla rekommendationer', 'kwpb_td' ),
  		'add_new_item'          => __( 'Lägg till ny rekommendation', 'kwpb_td' ),
  		'add_new'               => __( 'Lägg till ny', 'kwpb_td' ),
  		'new_item'              => __( 'Ny rekommendation', 'kwpb_td' ),
  		'edit_item'             => __( 'Redigera rekommendation', 'kwpb_td' ),
  		'update_item'           => __( 'Uppdatera rekommendation', 'kwpb_td' ),
  		'view_item'             => __( 'Visa rekommendation', 'kwpb_td' ),
  		'view_items'            => __( 'Visa rekommendationer', 'kwpb_td' ),
  		'search_items'          => __( 'Sök rekommendation', 'kwpb_td' ),
  	);
  	$args = array(
  		'label'                 => __( 'Testimonial', 'kwpb_td' ),
  		'description'           => __( 'Functionality to manage, list and display testimonials.', 'kwpb_td' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'editor' ),
  		'hierarchical'          => false,
  		'public'                => true,
      'show_ui'               => true,
  		'show_in_menu'          => 'kwpb-options',
      'show_in_rest'          => true,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => true,
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'kprl_testimonial', $args );

  }
  add_action( 'init', 'kprl_testimonial_func', 0 );

}
