<?php

if ( ! function_exists( 'kprl_staffing_department_func' ) ) {

  // Register Custom Taxonomy
  function kprl_staffing_department_func() {

  	$labels = array(
  		'name'                       => _x( 'Departments', 'Taxonomy General Name', 'kwpb_td' ),
  		'singular_name'              => _x( 'Department', 'Taxonomy Singular Name', 'kwpb_td' ),
  		'menu_name'                  => __( 'Departments', 'kwpb_td' ),
  		'all_items'                  => __( 'All Items', 'kwpb_td' ),
  		'parent_item'                => __( 'Parent Item', 'kwpb_td' ),
  		'parent_item_colon'          => __( 'Parent Item:', 'kwpb_td' ),
  		'new_item_name'              => __( 'New Item Name', 'kwpb_td' ),
  		'add_new_item'               => __( 'Add New Item', 'kwpb_td' ),
  		'edit_item'                  => __( 'Edit Item', 'kwpb_td' ),
  		'update_item'                => __( 'Update Item', 'kwpb_td' ),
  		'view_item'                  => __( 'View Item', 'kwpb_td' ),
  		'separate_items_with_commas' => __( 'Separate items with commas', 'kwpb_td' ),
  		'add_or_remove_items'        => __( 'Add or remove items', 'kwpb_td' ),
  		'choose_from_most_used'      => __( 'Choose from the most used', 'kwpb_td' ),
  		'popular_items'              => __( 'Popular Items', 'kwpb_td' ),
  		'search_items'               => __( 'Search Items', 'kwpb_td' ),
  		'not_found'                  => __( 'Not Found', 'kwpb_td' ),
  		'no_terms'                   => __( 'No items', 'kwpb_td' ),
  		'items_list'                 => __( 'Items list', 'kwpb_td' ),
  		'items_list_navigation'      => __( 'Items list navigation', 'kwpb_td' ),
  	);
  	$args = array(
  		'labels'                     => $labels,
  		'hierarchical'               => true,
  		'public'                     => true,
  		'show_ui'                    => true,
      'show_in_menu'               => 'kwpb-options',
  		'show_admin_column'          => true,
  		'show_in_nav_menus'          => true,
  		'show_tagcloud'              => true,
  	);
  	register_taxonomy( 'kprl_staffing_department', array( 'kprl_staffing' ), $args );

  }
  add_action( 'init', 'kprl_staffing_department_func', 0 );

}

if ( ! function_exists('kprl_staffing_func') ) {

  // Register Custom Post Type
  function kprl_staffing_func() {

  	$labels = array(
  		'name'                  => _x( 'Personer', 'Post Type General Name', 'kwpb_td' ),
  		'singular_name'         => _x( 'Person', 'Post Type Singular Name', 'kwpb_td' ),
  		'menu_name'             => __( 'Personer', 'kwpb_td' ),
  		'name_admin_bar'        => __( 'Personer', 'kwpb_td' ),
  		'archives'              => __( 'Person-arkiv', 'kwpb_td' ),
  		'attributes'            => __( 'Person-attribut', 'kwpb_td' ),
  		'parent_item_colon'     => __( 'Person-förälder:', 'kwpb_td' ),
  		'all_items'             => __( 'Alla personer', 'kwpb_td' ),
  		'add_new_item'          => __( 'Lägg till ny person', 'kwpb_td' ),
  		'add_new'               => __( 'Lägg till ny', 'kwpb_td' ),
  		'new_item'              => __( 'Ny person', 'kwpb_td' ),
  		'edit_item'             => __( 'Redigera person', 'kwpb_td' ),
  		'update_item'           => __( 'Uppdatera person', 'kwpb_td' ),
  		'view_item'             => __( 'Visa person', 'kwpb_td' ),
  		'view_items'            => __( 'Visa personer', 'kwpb_td' ),
  		'search_items'          => __( 'Sök person', 'kwpb_td' ),
  	);
  	$args = array(
  		'label'                 => __( 'Staff', 'kwpb_td' ),
  		'description'           => __( 'Functionality to manage, list and display staff and individuals based on departments and other categories and criterias.', 'kwpb_td' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'thumbnail', 'editor' ),
  		'taxonomies'            => array( 'kprl_staffing_department' ),
  		'hierarchical'          => false,
  		'public'                => true,
      'show_ui'               => true,
  		'show_in_menu'          => 'kwpb-options',
      'show_in_rest'          => true,
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => false,
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  	);
  	register_post_type( 'kprl_staffing', $args );

  }
  add_action( 'init', 'kprl_staffing_func', 0 );

}

function return_contact_array( $key = false ) {

  //email
  $contactArray['email']['key']               = 'email';
  $contactArray['email']['title']             = 'Epost';
  $contactArray['email']['pre_username']      = '';
  $contactArray['email']['class']             = 'email';
  $contactArray['email']['color']             = '#d14836';
  $contactArray['email']['svg']               = '<svg viewBox="0 0 32 32" class="icon icon-mail" viewBox="0 0 32 32" aria-hidden="true"><path d="M5 24.225V7.776h22v16.447H5v.002zm3.011-1.815h15.978l-5.111-5.115L16 20.179l-2.877-2.883-5.112 5.114zm-1.216-1.275l5.077-5.09-5.077-5.065v10.155zm13.332-5.09l5.079 5.09V10.979l-5.079 5.066zm-4.126 1.588l8.022-8.027-16.045-.001 8.023 8.028z"/></svg>';

  //phone
  $contactArray['phone']['key']               = 'phone';
  $contactArray['phone']['title']             = 'Telefon';
  $contactArray['phone']['pre_username']      = '';
  $contactArray['phone']['class']             = 'phone';
  $contactArray['phone']['color']             = '#7c7c7c';
  $contactArray['phone']['svg']               = '<svg viewBox="0 0 32 32" class="icon icon-phone" viewBox="0 0 32 32" aria-hidden="true"><path d="M11.748 5.773S11.418 5 10.914 5c-.496 0-.754.229-.926.387S6.938 7.91 6.938 7.91s-.837.731-.773 2.106c.054 1.375.323 3.332 1.719 6.058 1.386 2.72 4.855 6.876 7.047 8.337 0 0 2.031 1.558 3.921 2.191.549.173 1.647.398 1.903.398.26 0 .719 0 1.246-.385.536-.389 3.543-2.807 3.543-2.807s.736-.665-.119-1.438c-.859-.773-3.467-2.492-4.025-2.944-.559-.459-1.355-.257-1.699.054-.343.313-.956.828-1.031.893-.112.086-.419.365-.763.226-.438-.173-2.234-1.148-3.899-3.426-1.655-2.276-1.837-3.02-2.084-3.824a.56.56 0 0 1 .225-.657c.248-.172 1.161-.933 1.161-.933s.591-.583.344-1.27-1.906-4.716-1.906-4.716z"/></svg>';

  //facebook
  $contactArray['facebook']['key']            = 'facebook';
  $contactArray['facebook']['title']          = 'Facebook';
  $contactArray['facebook']['pre_username']   = '/';
  $contactArray['facebook']['class']          = 'facebook';
  $contactArray['facebook']['color']          = '#3B5999';
  $contactArray['facebook']['svg']            = '<svg viewBox="0 0 32 32" class="icon icon-facebook" viewBox="0 0 32 32" aria-hidden="true"><path d="M21.95 5.005l-3.306-.004c-3.206 0-5.277 2.124-5.277 5.415v2.495H10.05v4.515h3.317l-.004 9.575h4.641l.004-9.575h3.806l-.003-4.514h-3.803v-2.117c0-1.018.241-1.533 1.566-1.533l2.366-.001.01-4.256z"/></svg>';

  //instagram
  $contactArray['instagram']['key']           = 'instagram';
  $contactArray['instagram']['title']         = 'Instagram';
  $contactArray['instagram']['pre_username']  = '@';
  $contactArray['instagram']['class']         = 'instagram';
  $contactArray['instagram']['color']         = '#E4405F';
  $contactArray['instagram']['svg']           = '<svg viewBox="0 0 32 32" class="icon icon-instagram" viewBox="0 0 32 32" aria-hidden="true"><path d="M20.445 5h-8.891A6.559 6.559 0 0 0 5 11.554v8.891A6.559 6.559 0 0 0 11.554 27h8.891a6.56 6.56 0 0 0 6.554-6.555v-8.891A6.557 6.557 0 0 0 20.445 5zm4.342 15.445a4.343 4.343 0 0 1-4.342 4.342h-8.891a4.341 4.341 0 0 1-4.341-4.342v-8.891a4.34 4.34 0 0 1 4.341-4.341h8.891a4.342 4.342 0 0 1 4.341 4.341l.001 8.891z"/><path d="M16 10.312c-3.138 0-5.688 2.551-5.688 5.688s2.551 5.688 5.688 5.688 5.688-2.551 5.688-5.688-2.55-5.688-5.688-5.688zm0 9.163a3.475 3.475 0 1 1-.001-6.95 3.475 3.475 0 0 1 .001 6.95zM21.7 8.991a1.363 1.363 0 1 1-1.364 1.364c0-.752.51-1.364 1.364-1.364z"/></svg>';

  //twitter
  $contactArray['twitter']['key']             = 'twitter';
  $contactArray['twitter']['title']           = 'Twitter';
  $contactArray['twitter']['pre_username']    = '@';
  $contactArray['twitter']['class']           = 'twitter';
  $contactArray['twitter']['color']           = '#55ACEE';
  $contactArray['twitter']['svg']             = '<svg viewBox="0 0 32 32" class="icon icon-twitter" viewBox="0 0 32 32" aria-hidden="true"><path d="M11.919 24.94c-2.548 0-4.921-.747-6.919-2.032a9.049 9.049 0 0 0 6.681-1.867 4.512 4.512 0 0 1-4.215-3.137c.276.054.559.082.848.082.412 0 .812-.056 1.193-.156a4.519 4.519 0 0 1-3.622-4.425v-.059a4.478 4.478 0 0 0 2.042.564 4.507 4.507 0 0 1-2.008-3.758c0-.824.225-1.602.612-2.268a12.811 12.811 0 0 0 9.303 4.715 4.517 4.517 0 0 1 7.692-4.115 9.107 9.107 0 0 0 2.866-1.094 4.542 4.542 0 0 1-1.983 2.498 9.08 9.08 0 0 0 2.592-.71 9.283 9.283 0 0 1-2.252 2.337c.008.193.014.388.014.583-.001 5.962-4.542 12.843-12.844 12.842"/></svg>';

  //skype
  $contactArray['skype']['key']               = 'skype';
  $contactArray['skype']['title']             = 'Skype';
  $contactArray['skype']['pre_username']      = '@';
  $contactArray['skype']['class']             = 'skype';
  $contactArray['skype']['color']             = '#00AFF0';
  $contactArray['skype']['svg']               = '<svg viewBox="0 0 32 32" class="icon icon-skype" viewBox="0 0 32 32" aria-hidden="true"><path d="M26.922 21.014c-.106-1.321-.616-2.223-1.089-2.794.161-.716.254-1.458.254-2.223 0-5.57-4.515-10.085-10.085-10.085-.765 0-1.507.093-2.223.254-.503-.439-1.908-1.416-4.251-1.086C6.542 5.5 4.792 8.417 5 11c.111 1.379.659 2.295 1.148 2.861a10.158 10.158 0 0 0-.231 2.136c0 5.57 4.515 10.086 10.085 10.086.736 0 1.452-.084 2.143-.233.506.44 1.91 1.413 4.249 1.084 2.985-.42 4.736-3.336 4.528-5.92zm-5.938-.248c-.69.774-1.604 1.548-3.797 1.828-3.672.469-5.781-1.312-6.344-1.859s-.656-1.911-.031-2.406c.828-.656 1.512-.545 2.781.75 1.562 1.594 4.093 1.163 4.516.281.359-.75-.078-1.328-.891-1.562-.859-.248-4.05-1.047-4.609-1.266-2.873-1.123-2.219-3.859-2.094-4.297s.456-2.329 4.047-2.891c3.592-.562 5.812 1.188 5.812 1.188 1.312 1.047.797 2.406.078 2.781s-1.328.25-2.312-.656-2.312-.797-2.625-.766-1.281.109-1.515.768.172 1.043.922 1.341c1.469.583 3.362.316 5.484 1.75 2.266 1.531 1.219 4.297.578 5.016z"/></svg>';

  //youtube
  $contactArray['youtube']['key']             = 'youtube';
  $contactArray['youtube']['title']           = 'YouTube';
  $contactArray['youtube']['pre_username']    = '/';
  $contactArray['youtube']['class']           = 'youtube';
  $contactArray['youtube']['color']           = '#CD201F';
  $contactArray['youtube']['svg']             = '<svg viewBox="0 0 32 32" class="icon icon-youtube" viewBox="0 0 32 32" aria-hidden="true"><path d="M22.396 21.104H21.2v-.707c0-.299.298-.597.596-.597h.103c.302 0 .603.298.603.597v.707h-.106zM17.795 19.6c-.298 0-.6.2-.6.498v3.405c0 .298.205.5.6.5.301 0 .602-.202.602-.5v-3.5c-.002-.203-.301-.403-.602-.403zm7.406-1.9v6.497c0 1.604-1.402 2.803-3.003 2.803H9.797c-1.6 0-2.998-1.3-2.998-2.803V17.7c0-1.599 1.301-2.802 2.998-2.802H22.1c1.699 0 3.101 1.204 3.101 2.802zm-14.502 7.204v-6.809H12.2v-.992H8.1v.992h1.299v6.899l1.3-.09zm4.501-5.8h-1.203v4.492c0 .204-.197.301-.397.301-.101 0-.302-.097-.302-.395v-4.506h-1.3V23.8c0 .298 0 .696.301.899.602.398 1.698-.096 1.901-.697v.795h1v-5.693zm4.098 4.098V20.2c0-1.203-.901-1.899-1.998-.902v-2.299h-1.302v7.799h1.001l.104-.495c1.4 1.295 2.195.397 2.195-1.101zm3.997-.402h-.99v.703c0 .298-.205.5-.509.5h-.198c-.302 0-.494-.202-.494-.5v-1.401h2.191v-2.396c-.193-1.104-1.697-1.309-2.492-.706-.203.202-.408.405-.5.706-.103.3-.207.691-.207 1.294v1.8c0 3.096 3.5 2.705 3.199 0zm-4.9-9.801c.107.203.201.3.303.399.104.104.301.104.501.104.204 0 .298-.104.504-.203.097-.097.3-.3.396-.498v.498H21.5V7.301h-1.104v4.698c0 .301-.192.502-.494.502-.301 0-.498-.201-.498-.502V7.4h-1.198v5.101c.093.197.093.3.189.498zm-4.297-3.397c0-.6 0-1 .103-1.401.097-.3.301-.599.499-.798a1.66 1.66 0 0 1 .999-.301c.3 0 .598.098.803.2.197.101.396.3.498.498.104.203.195.401.301.603 0 .199.102.5.102.998V11c0 .602 0 .999-.102 1.202 0 .299-.104.496-.301.698a1.237 1.237 0 0 1-.498.498 1.86 1.86 0 0 1-.803.201c-.298 0-.599 0-.8-.099-.199-.103-.397-.203-.5-.399a5.108 5.108 0 0 1-.299-.703c-.102-.299-.102-.7-.102-1.197l.1-1.599zm1.102 2.397c0 .301.301.603.601.603.301 0 .596-.302.596-.603V8.798c0-.301-.295-.599-.596-.599-.3 0-.601.298-.601.599v3.201zM11.3 13.6h1.3V9.001L14.2 5h-1.503L11.9 8.001 10.999 5H9.6l1.7 4.001V13.6z"/></svg>';

  //github
  $contactArray['github']['key']              = 'github';
  $contactArray['github']['title']            = 'GitHub';
  $contactArray['github']['pre_username']     = '/';
  $contactArray['github']['class']            = 'github';
  $contactArray['github']['color']            = '#24292e';
  $contactArray['github']['svg']              = '<svg viewBox="0 0 32 32" class="icon icon-github" viewBox="0 0 32 32" aria-hidden="true"><path d="M24.92 12.183c0-1.586-.604-2.864-1.585-3.83.172-.547.398-1.763-.229-3.321 0 0-1.114-.348-3.628 1.315a12.695 12.695 0 0 0-3.081-.366c-1.154 0-2.322.143-3.409.44-2.596-1.747-3.74-1.391-3.74-1.391-.748 1.847-.287 3.215-.145 3.554-.883.936-1.414 2.133-1.414 3.594 0 1.111.128 2.099.44 2.964l.325.732c.879 1.614 2.606 2.655 5.677 2.983-.434.289-.885.779-1.062 1.612-.594.28-2.475.966-3.603-.944 0 0-.633-1.148-1.842-1.235 0 0-1.174-.017-.08.722 0 0 .782.367 1.326 1.738 0 0 .705 2.342 4.114 1.593v2.417s-.076.857-.867 1.143c0 0-.469.312.034.497 0 0 2.205.174 2.205-1.604v-2.643s-.09-1.047.429-1.404v4.332s-.032 1.031-.576 1.421c0 0-.362.646.433.468 0 0 1.517-.211 1.584-1.967l.035-4.383h.363l.033 4.383c.076 1.748 1.59 1.967 1.59 1.967.793.179.429-.468.429-.468-.54-.389-.579-1.421-.579-1.421v-4.297c.52.402.436 1.369.436 1.369v2.643c0 1.777 2.2 1.604 2.2 1.604.505-.186.036-.498.036-.498-.793-.286-.867-1.143-.867-1.143v-3.461c0-1.346-.574-2.056-1.137-2.435 3.277-.318 4.845-1.368 5.572-2.99-.015.027.26-.726.26-.726.25-.859.325-1.855.325-2.963h-.002z"/></svg>';

  if ( $key ) {
    return $contactArray[$key];
  } else {
    return $contactArray;
  }

}

function return_staffing_array( $content = null, $value = null ) {

  if ( $content == "specific-departments" ) {
    $categories = array(
                    array(
                      'taxonomy' => 'kprl_staffing_department',
                      'field' => 'id',
                      'terms' => $value // Where term_id of Term 1 is "1".
                    )
                  );
  } else {
    $categories = array();
  }

  $stfArr = get_posts(array(
    'post_type' 		  => 'kprl_staffing',
    'posts_per_page'  => -1,
    'orderby'         => 'date',
    'order'           => 'DESC',
    'status'          => 'published',
    'tax_query' 	    => $categories,
  ));

  if ( $content == "specific-staffing" ) {
    if ($value) {
      $stfArr = $value;
    }
  }

  foreach ($stfArr as $key => $stf) {
    $staffingArr[$key]['ID']            = $stf->ID;
    $staffingArr[$key]['title']         = $stf->post_title;
    $staffingArr[$key]['content']       = wpautop( $stf->post_content );
    $staffingArr[$key]['thumbnail_id']  = get_post_thumbnail_id( $stf->ID );
    $staffingArr[$key]['contact']       = null;
    $staffingArr[$key]['departments']   = null;

    //$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
    $args = array();
    $departments = wp_get_post_terms( $stf->ID, 'kprl_staffing_department', $args );

    if ( is_array($departments) ) {
      foreach ($departments as $dkey => $d) {
        $staffingArr[$key]['departments'][$dkey]['ID']          = $d->term_id;
        $staffingArr[$key]['departments'][$dkey]['title']       = $d->name;
        $staffingArr[$key]['departments'][$dkey]['description'] = $d->description;
        $staffingArr[$key]['departments'][$dkey]['parent']      = $d->parent;
        $staffingArr[$key]['departments'][$dkey]['count']       = $d->count;
      }
    }

    $fields = get_fields($stf->ID);

    if ( is_array($fields['kontaktinformation']) ) {

      $contactArray = return_contact_array();
      foreach ($fields['kontaktinformation'] as $ckey => $c) {

        $layout = $c['acf_fc_layout'];
        $staffingArr[$key]['contact'][$ckey]['key']           = $layout;
        $staffingArr[$key]['contact'][$ckey]['title']         = $contactArray[$layout]['title'];
        $staffingArr[$key]['contact'][$ckey]['pre_username']  = $contactArray[$layout]['svg'];
        $staffingArr[$key]['contact'][$ckey]['class']         = $contactArray[$layout]['class'];
        $staffingArr[$key]['contact'][$ckey]['color']         = $contactArray[$layout]['color'];
        $staffingArr[$key]['contact'][$ckey]['svg']           = $contactArray[$layout]['svg'];

        if ( $layout == 'facebook' OR $layout == 'instagram' OR $layout == 'twitter' OR $layout == 'youtube' OR $layout == 'skype' OR $layout == 'github' OR $layout == 'bitbucket' ) {
          $staffingArr[$key]['contact'][$ckey]['value']['link']     = $c['link']['url'];
          $staffingArr[$key]['contact'][$ckey]['value']['target']   = $c['link']['target'];
          $staffingArr[$key]['contact'][$ckey]['value']['display']  = $contactArray[$layout]['pre_username'] . $c['username'];
        } else if ( $layout == 'phone' ) {
          $staffingArr[$key]['contact'][$ckey]['value']['link']     = "tel:" . $c['phonenumber'];
          $staffingArr[$key]['contact'][$ckey]['value']['target']   = null;
          $staffingArr[$key]['contact'][$ckey]['value']['number']   = $c['phonenumber'];

          $nummer = $c['phonenumber'];
          $nummer = str_replace("+46", "0", $nummer);
          $nummer = substr_replace($nummer, '-', 3, 0);
          $nummer = substr_replace($nummer, ' ', 7, 0);
          $nummer = substr_replace($nummer, ' ', 10, 0);

          $staffingArr[$key]['contact'][$ckey]['value']['display']  = $nummer;
        } else if ( $layout == 'email' ) {
          $staffingArr[$key]['contact'][$ckey]['value']['link']     = "mailto:" . $c['email'];
          $staffingArr[$key]['contact'][$ckey]['value']['target']   = null;
          $staffingArr[$key]['contact'][$ckey]['value']['email']    = $c['email'];
          $staffingArr[$key]['contact'][$ckey]['value']['display']  = $c['email'];
        }
      }
    }

    $contactArray = array();
    $fields = array();
  }

  return $staffingArr;

}
