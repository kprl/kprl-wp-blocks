=== kprl WP Blocks ===
Contributors: dansarn
Requires at least: 5.0
Tested up to: 5.1
Requires PHP: 7.0

Functionality to add useful Guthenberg-blocks.
