<?php
/**
  * Plugin Name:          kprl WP Blocks
  * Plugin URI:           https://bitbucket.org/kprl/kprl-wp-blocks
  * Bitbucket Plugin URI: https://bitbucket.org/kprl/kprl-wp-blocks
  * Description:          Functionality to add useful Guthenberg-blocks.
  * Version:              1.1.6
  * Author:               Karl Lettenström
  * Author URI:           http://kprl.se
  * Text Domain:          kwpb_td
  * Domain Path:          Optional. Plugin's relative directory path to .mo files. Example: /locale/
  * Network:              Optional. Whether the plugin can only be activated network wide. Example: true
  * License:              GPL2
	* Requires PHP: 				7.0
 */

/*  Copyright 2019 Karl Lettenström (email : karl@kprl.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'KWPB_PLUGIN_VERSION' ) ) {
	define( 'KWPB_PLUGIN_VERSION', '1.1.6' );
}

if ( ! defined( 'KWPB_PLUGIN_URI' ) ) {
	define( 'KWPB_PLUGIN_URI', 'kprl-wp-blocks' );
}

if ( ! defined( 'KWPB_PLUGIN_NAME' ) ) {
	define( 'KWPB_PLUGIN_NAME', 'kprl WP Blocks' );
}

if ( ! defined( 'KWPB_PLUGIN_SHORT' ) ) {
	define( 'KWPB_PLUGIN_SHORT', 'KWPB' );
}

if ( ! defined( 'KWPB_PLUGIN_HEADER' ) ) {
	define( 'KWPB_PLUGIN_HEADER', '<h5 class="kwpb_plugin_version">' . KWPB_PLUGIN_NAME . ', version ' . KWPB_PLUGIN_VERSION . '</h5>' );
}

if ( ! defined( 'KWPB_PLUGIN_CORE' ) ) {
	define( 'KWPB_PLUGIN_CORE', plugin_dir_path( __FILE__ ) . 'core' );
}

if ( ! defined( 'KWPB_PLUGIN_CPT' ) ) {
	define( 'KWPB_PLUGIN_CPT', plugin_dir_path( __FILE__ ) . 'cpt' );
}

if ( ! defined( 'KWPB_PLUGIN_ASSETS' ) ) {
	define( 'KWPB_PLUGIN_ASSETS', plugin_dir_path( __FILE__ ) . 'assets' );
}

if ( ! defined( 'KWPB_PLUGIN_IMAGE' ) ) {
	define( 'KWPB_PLUGIN_IMAGE', plugins_url() . '/' . KWPB_PLUGIN_URI . '/assets/image' );
}

if ( ! defined( 'KWPB_PLUGIN_CSS' ) ) {
	define( 'KWPB_PLUGIN_CSS', plugins_url() . '/' . KWPB_PLUGIN_URI . '/assets/css' );
}

if ( ! defined( 'KWPB_PLUGIN_JS' ) ) {
	define( 'KWPB_PLUGIN_JS', plugins_url() . '/' . KWPB_PLUGIN_URI . '/assets/js' );
}

if ( ! defined( 'KWPB_PLUGIN_TEMPLATE' ) ) {
	define( 'KWPB_PLUGIN_TEMPLATE', plugin_dir_path( __FILE__ ) . 'template-parts' );
}

function kwpb_custom_admin_style() {
	wp_enqueue_style( 'kwpb_admin_style', KWPB_PLUGIN_CSS . '/kwpb_admin_style.css?v=' . KWPB_PLUGIN_VERSION );
}
add_action('admin_head', 'kwpb_custom_admin_style');

function kwpb_custom_frontend_style() {
	wp_enqueue_style( 'kwpb_frontend_style', KWPB_PLUGIN_CSS . '/kwpb_frontend_style.css?v=' . KWPB_PLUGIN_VERSION );
}
add_action('wp_enqueue_scripts', 'kwpb_custom_frontend_style');

//core-files
include KWPB_PLUGIN_CORE . '/functions.php';

//cpt-files
include KWPB_PLUGIN_CPT . '/kprl-staffing.php';
//include KWPB_PLUGIN_CPT . '/kprl-testimonial.php';

/*** För en gemensam menykategori i adminlägets vänsterspalt. ***/
function kwpb_add_admin_menu() {

  add_menu_page(
    __('kprl', 'kwpb_td'),
    __('kprl', 'kwpb_td'),
    'manage_options',
    'kwpb-options',
    '',
    KWPB_PLUGIN_IMAGE . '/kprl_rgb_16_2.png',
    3
    );

	//Inställningar och information
  add_submenu_page(
    'kwpb-options',
    __('Inställningar', 'kwpb_td'),
    __('Inställningar', 'kwpb_td'),
    'manage_options',
    'kwpb-options',
    'kwpb_options_page'
    );

}
add_action( 'admin_menu', 'kwpb_add_admin_menu' );
