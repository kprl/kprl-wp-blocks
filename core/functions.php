<?php

if ( ! function_exists( 'pre_r' ) ) {
  function pre_r( $val, $die = 0 ) {
    echo "<pre>";
    print_r( $val );
    echo "</pre>";
    if ( $die == 1 ) { die(); }
  }
}

if ( ! function_exists( 'kprl_load_bootstrap' ) ) {
	function kprl_load_bootstrap() {
		wp_enqueue_style( 'fontawesome', 'https://use.fontawesome.com/releases/v5.7.2/css/all.css' );
		wp_enqueue_style( 'bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
    wp_enqueue_script( 'boot1','https://code.jquery.com/jquery-3.4.1.min.js', '', '', true );
		wp_enqueue_script( 'boot2','https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', '', '', true );
		wp_enqueue_script( 'boot3','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', '', '', true );
	}
	add_action( 'wp_enqueue_scripts', 'kprl_load_bootstrap' );
}

function kwpb_acf_init() {

  /** BLOCKS **/
  // check function exists
  if ( function_exists('acf_register_block') ) {

    acf_register_block(array(
      'name'            => 'kprl-image-gallery',
      'title'           => __('kprl Image Gallery'),
      'description'		  => __('A image-gallery block.'),
      'render_template' => KWPB_PLUGIN_TEMPLATE . '/block-kprl-image-gallery.php',
      'mode'            => 'auto',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'format-gallery',
      'keywords'        => array( 'kprl', 'carousel', 'bootstrap', 'bildspel', 'gallery', 'image', 'columns' ),
    ));

    acf_register_block(array(
      'name'            => 'kprl-bootstrap-carousel',
      'title'           => __('kprl Bootstrap Carousel'),
      'description'		  => __('A Bootstrap carousel block.'),
      'render_template' => KWPB_PLUGIN_TEMPLATE . '/block-kprl-bootstrap-carousel.php',
      'mode'            => 'auto',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'slides',
      'keywords'        => array( 'carousel', 'bootstrap', 'bildspel', 'kprl' ),
    ));

    acf_register_block(array(
      'name'            => 'kprl-bootstrap-jumbotron',
      'title'           => __('kprl Bootstrap Jumbotron'),
      'description'		  => __('A Bootstrap jumbotron block.'),
      'render_template' => KWPB_PLUGIN_TEMPLATE . '/block-kprl-bootstrap-jumbotron.php',
      'mode'            => 'auto',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'jumbotron', 'bootstrap', 'kprl' ),
    ));

		acf_register_block(array(
      'name'            => 'kprl-bootstrap-media-list',
      'title'           => __('kprl Bootstrap Media list'),
      'description'		  => __('A Bootstrap Media list block.'),
      'render_template' => KWPB_PLUGIN_TEMPLATE . '/block-kprl-bootstrap-media-list.php',
      'enqueue_style'   => KWPB_PLUGIN_CSS . '/kprl-bootstrap-media-list.css',
      'mode'            => 'preview',
      'supports'        => array(
                            'align' => array( 'left', 'wide' ),
                           ),
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'media list', 'media', 'list', 'bootstrap', 'kprl' ),
    ));

    acf_register_block(array(
      'name'            => 'kprl-staffing',
      'title'           => __('kprl Bemanning'),
      'description'		  => __('A staffing-block.'),
      'render_template' => KWPB_PLUGIN_TEMPLATE . '/block-kprl-staffing.php',
      'enqueue_style'   => KWPB_PLUGIN_CSS . '/kprl-staffing.css',
      'enqueue_script'  => KWPB_PLUGIN_JS . '/kprl-staffing.js',
      'mode'            => 'preview',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'bemanning', 'staffing', 'kprl' ),
    ));

  }

  global $kwpb_bootstrapMediaListPosttype;
  $kwpb_bootstrapMediaListPosttype['posts'] = 'kwpb_youtube';
  $kwpb_bootstrapMediaListPosttype['post']  = 'kwpb_youtube';
}
add_action('acf/init', 'kwpb_acf_init');

add_action('init', 'load_kwpb_acf_field_group');
function load_kwpb_acf_field_group() {
  if (function_exists('register_field_group')) {
		if ( file_exists( KWPB_PLUGIN_CORE . "/acf_include.php" ) ) {
			require_once( KWPB_PLUGIN_CORE . "/acf_include.php" );
		}
  }
}

function hex2rgba($color, $opacity = false) {

	$default = 'rgb(0,0,0)';

	//Return default if no color provided
	if(empty($color))
          return $default;

	//Sanitize $color if "#" is provided
  if ($color[0] == '#' ) {
  	$color = substr( $color, 1 );
  }

  //Check if color has 6 or 3 characters and get values
  if (strlen($color) == 6) {
    $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
  } elseif ( strlen( $color ) == 3 ) {
    $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
  } else {
    return $default;
  }

  //Convert hexadec to rgb
  $rgb =  array_map('hexdec', $hex);

  //Check if opacity is set(rgba or rgb)
  if($opacity){
  	if(abs($opacity) > 1)
  		$opacity = 1.0;
  	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
  } else {
  	$output = 'rgb('.implode(",",$rgb).')';
  }

  //Return rgb(a) color string
  return $output;
}

function display_kwpb_media_thumb( $pid, $pt, $link = false, $size = 'full' ){
  $stream = "";
  global $kwpb_bootstrapMediaListPosttype;

  if ( !array_key_exists($pt, $kwpb_bootstrapMediaListPosttype) ) {
    $pt = 'post';
  }

  if ( get_post_meta( $pid, $kwpb_bootstrapMediaListPosttype[$pt], true ) ) {
		$stream = get_post_meta( $pid, $kwpb_bootstrapMediaListPosttype[$pt] );
	}

  if ( is_array($stream) AND function_exists('display_kwpb_stream_by_url') ) {
		display_kwpb_stream_by_url( $stream[0] );
	} else {
    if ( $link ) {
      echo '<a href="' . $link . '">' . get_the_post_thumbnail( $pid, $size ) . '</a>';
    } else {
      echo get_the_post_thumbnail( $pid, $size );
    }
	}
}

function display_kwpb_stream_by_url( $url = false, $class = "alignwide", $w = 640, $h = 360 ) {

  if ( $url ) {

    if ( strpos( $url, 'https://youtu.be/' ) !== false) {
      display_kwpb_youtube_by_url( $url, $class, $w, $h );
    } else if ( strpos( $url, 'https://www.youtube.com/watch?v=' ) !== false) {
      display_kwpb_youtube_by_url( $url, $class, $w, $h );
    } else if ( strpos( $url, 'https://www.facebook.com/' ) !== false) {
      display_kwpb_facebook_by_url( $url, $class, $w, $h );
    }

  }

}

function display_kwpb_youtube_by_url( $url = false, $class = "alignwide", $w = 640, $h = 360 ) {

  $str = "";

  if ( $url ) {

    if ( strpos( $url, 'https://youtu.be/' ) !== false) {
      $youtube_uid = explode("https://youtu.be/", $url);
    } else if ( strpos( $url, 'https://www.youtube.com/watch?v=' ) !== false) {
      $youtube_uid = explode("https://www.youtube.com/watch?v=", $url);
    }

    $str .= '<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio ' . $class . '"><div class="wp-block-embed__wrapper">';
    $str .= '<iframe width="' . $w . '" height="' . $h . '" src="https://www.youtube.com/embed/' . $youtube_uid[1] . '?feature=oembed" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>';
    $str .= '</div></figure>';
  }

  echo $str;
}

function display_kwpb_facebook_by_url( $url = false, $class = "alignwide", $w = 640, $h = 360 ) {

  $str = "";

  if ( $url ) {

    $find       = array( 'http://', 'https://' );
    $replace    = '';
    $permalink  = str_replace( $find, $replace, get_home_url() );

    $str .= '<figure class="wp-block-embed-facebook ' . $class . ' wp-block-embed is-type-video is-provider-facebook"><div class="wp-block-embed__wrapper">';
    $str .= '<div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; width: 0px; height: 0px;"><div>';
    $str .= '<iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter.php?version=44#channel=f2e5b663fb30de4&amp;origin=' . get_home_url() . '"  style="border: none;"></iframe>';
    $str .= '</div><div></div></div></div><script async="1" defer="1" crossorigin="anonymous" src="https://connect.facebook.net/sv_SE/sdk.js#xfbml=1&amp;version=v3.3"></script><div class="fb-video fb_iframe_widget fb_iframe_widget_fluid_desktop" data-href="' . $url . '" data-width="' . $w . '" fb-xfbml-state="rendered" fb-iframe-plugin-query="app_id=&amp;container_width=1020&amp;href=' . $url . '&amp;locale=sv_SE&amp;sdk=joey&amp;width=' . $w . '">';
    $str .= '<span style="vertical-align: bottom; width: ' . $w . '; height: ' . $h . ';">';
    $str .= '<iframe name="f3b4632141d3618" width="' . $w . '" height="' . $h . '" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" title="fb:video Facebook Social Plugin" src="https://www.facebook.com/v3.3/plugins/video.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D44%23cb%3Dffe7dbf3c5bd78%26domain%3D' . $permalink . '%26origin%3D' . get_home_url() . '%252Ff2e5b663fb30de4%26relation%3Dparent.parent&amp;container_width=1020&amp;href=' . $url . '&amp;locale=sv_SE&amp;sdk=joey&amp;width=' . $w . '" style="border: none; visibility: visible; width: ' . $w . 'px; height: ' . $h . 'px;" class=""></iframe>';
    $str .= '</span></div>';
    $str .= '</div></figure>';

  }

  echo $str;
}

function get_kprl_image_gallery( $array, $cols = 6 ) {
  $images = array();
  if ( is_array( $array[0] ) ) {
    if ( array_key_exists( 'ID', $array[0] ) ) {
      foreach ($array as $key => $img) {
        $images[] = $img['ID'];
      }
    }
  } else {
    $images = $array;
  }

  $str = '<ul class="apartment-thumbnails wp-block-gallery alignwide columns-' . $cols . ' is-cropped">';
  foreach ($images as $key => $img) {
    $class = array( 'class' => '' );
    $imgsrc = wp_get_attachment_image_src( $img, 'full' );

    $str .= '<li class="blocks-gallery-item">';
      $str .= '<a class="d-block" href="' . $imgsrc[0] . '">';
        $str .= wp_get_attachment_image( $img, 'ciab-topimage-medium', false, $class );
      $str .= '</a>';
    $str .= '</li>';

  }
  $str .= '</ul>';

  echo $str;
}

function return_kprl_human_time_string( $date = '1987-07-10 10:10:10', $limit = NULL ) {

  // array('second','minute','hour','day', 'week', 'month',  'year',   'decade');
  // array( 1,       60,      3600,  86400, 604800, 2630880,  31570560, 315705600);

  $diff = current_time('timestamp') - strtotime($date);

  if ( $diff >= $limit ) {
    return date("Y-m-d", strtotime($date));
  } else {
    return human_time_diff( strtotime($date), current_time('timestamp') ) . ' sedan';
  }
}
