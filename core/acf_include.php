<?php

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5c49efd627fd7',
	'title' => 'Block: kprl Bootstrap Carousel',
	'fields' => array(
		array(
			'key' => 'field_5c49fb50adb12',
			'label' => 'Bild-galleri',
			'name' => 'gallery',
			'type' => 'gallery',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 1,
			'max' => '',
			'insert' => 'append',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
			'return_format' => 'array',
			'preview_size' => 'medium',
		),
		array(
			'key' => 'field_5c4ad20de7029',
			'label' => 'Stil',
			'name' => 'style',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'slide' => 'Slide',
				'fade' => 'Fade',
			),
			'allow_null' => 0,
			'default_value' => 'slide',
			'layout' => 'horizontal',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5c4ad7e654240',
			'label' => 'Kontroller',
			'name' => 'controls',
			'type' => 'true_false',
			'instructions' => 'Visa föregående/nästa-kontroller?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c4ad89f350be',
			'label' => 'Indikatorer',
			'name' => 'indicators',
			'type' => 'true_false',
			'instructions' => 'Visa indikatorer?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c4ad99e06545',
			'label' => 'Bildtitel',
			'name' => 'image_title',
			'type' => 'true_false',
			'instructions' => 'Visa bildtitel?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
		array(
			'key' => 'field_5c4ad9d706546',
			'label' => 'Bildtext',
			'name' => 'image_text',
			'type' => 'true_false',
			'instructions' => 'Visa bildtext?',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '50',
				'class' => '',
				'id' => '',
			),
			'message' => '',
			'default_value' => 1,
			'ui' => 0,
			'ui_on_text' => '',
			'ui_off_text' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/kprl-bootstrap-carousel',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5c5c410471aa0',
	'title' => 'Block: kprl Bootstrap Jumbotron',
	'fields' => array(
		array(
			'key' => 'field_5c5c42c1c8199',
			'label' => 'Titel',
			'name' => 'jumbotron_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5c7fb8189e452',
			'label' => 'Bild',
			'name' => 'jumbotron_image',
			'type' => 'image',
			'instructions' => 'Ange eventuell bild',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => 'jpg,jpeg,png,gif',
		),
		array(
			'key' => 'field_5c5c432279309',
			'label' => 'Text',
			'name' => 'jumbotron_text',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 4,
			'new_lines' => '',
		),
		array(
			'key' => 'field_5c5c4365348c0',
			'label' => 'Knapptext',
			'name' => 'jumbotron_button_text',
			'type' => 'text',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5c5c4389fc0d2',
			'label' => 'Länk',
			'name' => 'jumbotron_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array(
			'key' => 'field_5c7fc885f7cf8',
			'label' => 'Öppna länk',
			'name' => 'jumbotron_link_target',
			'type' => 'radio',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'_top' => 'Öppna över befintlig sida',
				'_blank' => 'Öppna som ny sida',
			),
			'allow_null' => 0,
			'other_choice' => 0,
			'default_value' => '_top',
			'layout' => 'horizontal',
			'return_format' => 'value',
			'save_other_choice' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/kprl-bootstrap-jumbotron',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5c8779db664bc',
	'title' => 'Block: kprl Bootstrap Media list',
	'fields' => array(
		array(
			'key' => 'field_5d0204cad45f9',
			'label' => 'Innehåll',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 1,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5cecdfc067fb5',
			'label' => 'Innehåll',
			'name' => 'display_content',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'hide_admin' => 0,
			'choices' => array(
				'category' => 'Kategori',
				'posttype' => 'Posttyp',
				'video' => 'Filmklipp',
			),
			'allow_null' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5c87889901b9e',
			'label' => 'Kategori',
			'name' => 'medialist_category',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'category',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layout' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_5c9b3eb2a5ed7',
					'label' => 'Alla eller utvalda?',
					'name' => 'display',
					'type' => 'button_group',
					'instructions' => 'Välj om du vill visa alla eller valda kategorier!',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'all' => 'Alla kategorier',
						'specific' => 'Specifika utvalda kategorier',
					),
					'allow_null' => 0,
					'default_value' => 'all',
					'layout' => 'vertical',
					'return_format' => 'value',
				),
				array(
					'key' => 'field_5c878006f3ee7',
					'label' => 'Välj kategori(er)',
					'name' => 'categories',
					'type' => 'taxonomy',
					'instructions' => 'Välj en eller flera kategorier att lista.',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5c9b3eb2a5ed7',
								'operator' => '==',
								'value' => 'specific',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'taxonomy' => 'category',
					'field_type' => 'checkbox',
					'add_term' => 0,
					'save_terms' => 0,
					'load_terms' => 0,
					'return_format' => 'id',
					'multiple' => 0,
					'allow_null' => 0,
				),
				array(
					'key' => 'field_5cab3f8ff43db',
					'label' => 'Kategori-beskrivning',
					'name' => 'category_description',
					'type' => 'true_false',
					'instructions' => 'Visa kategorins beskrivande text?',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
			),
		),
		array(
			'key' => 'field_5cecdf0785d83',
			'label' => 'Posttyp',
			'name' => 'medialist_posttype',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'posttype',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layout' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_5cecdf0785d84',
					'label' => 'Alla eller utvalda?',
					'name' => 'display',
					'type' => 'button_group',
					'instructions' => 'Välj om du vill visa alla eller valda kategorier!',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'all' => 'Alla posttyper',
						'specific' => 'Specifika utvalda posttyper',
					),
					'allow_null' => 0,
					'default_value' => 'all',
					'layout' => 'vertical',
					'return_format' => 'value',
				),
				array(
					'key' => 'field_5cecdf0785d85',
					'label' => 'Välj posttyp(er)',
					'name' => 'posttype',
					'type' => 'button_group',
					'instructions' => 'Välj en eller flera posttyper att lista.',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5cecdf0785d84',
								'operator' => '==',
								'value' => 'specific',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						'post' => 'Inlägg',
						'glimt_livestream' => 'Livestream',
					),
					'allow_null' => 0,
					'default_value' => '',
					'layout' => 'vertical',
					'return_format' => 'value',
				),
			),
		),
		array(
			'key' => 'field_5e39980636521',
			'label' => 'Videolista',
			'name' => 'medialist_videolist',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'video',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'hide_admin' => 0,
			'layout' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_5e39980636522',
					'label' => 'Alla eller utvalda?',
					'name' => 'display',
					'type' => 'button_group',
					'instructions' => 'Välj om du vill visa alla eller valda mappar!',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'hide_admin' => 0,
					'choices' => array(
						'all' => 'Alla mappar',
						'specific' => 'Specifika utvalda mappar',
					),
					'allow_null' => 0,
					'default_value' => 'all',
					'layout' => 'vertical',
					'return_format' => 'value',
				),
				array(
					'key' => 'field_5e39980636523',
					'label' => 'Välj mapp(ar)',
					'name' => 'folders',
					'type' => 'taxonomy',
					'instructions' => 'Välj en eller flera mappar att lista.',
					'required' => 1,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5e39980636522',
								'operator' => '==',
								'value' => 'specific',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'hide_admin' => 0,
					'taxonomy' => 'mediamatic_wpfolder',
					'field_type' => 'checkbox',
					'add_term' => 0,
					'save_terms' => 0,
					'load_terms' => 0,
					'return_format' => 'id',
					'multiple' => 0,
					'allow_null' => 0,
				),
				array(
					'key' => 'field_5e39980636524',
					'label' => 'Mapp-beskrivning',
					'name' => 'folder_description',
					'type' => 'true_false',
					'instructions' => 'Visa mappens beskrivande text?',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'hide_admin' => 0,
					'message' => '',
					'default_value' => 0,
					'ui' => 0,
					'ui_on_text' => '',
					'ui_off_text' => '',
				),
			),
		),
		array(
			'key' => 'field_5d0204f2d45fa',
			'label' => 'Utseende',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5d01fa767810b',
			'label' => 'Utseende',
			'name' => 'display_layout',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'category',
					),
				),
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'posttype',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => 'fullwidth',
				'id' => '',
			),
			'hide_admin' => 0,
			'choices' => array(
				'1ca' => 'Enspalt (lista under)',
				'1cb' => 'Enspalt (lista ovanför)',
				'2ca' => 'Tvåspalt (bred lista höger)',
				'2cb' => 'Tvåspalt (smal lista höger)',
				'2cc' => 'Tvåspalt (bred lista vänster)',
				'2cd' => 'Tvåspalt (smal lista vänster)',
				'2ce' => 'Tvåspalt (två stora klipp, lista under)',
			),
			'allow_null' => 0,
			'default_value' => '2ca',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5e3ac7f2e5c1b',
			'label' => 'Utseende',
			'name' => 'display_layout_videolist',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cecdfc067fb5',
						'operator' => '==',
						'value' => 'video',
					),
				),
			),
			'wrapper' => array(
				'width' => '100',
				'class' => 'fullwidth',
				'id' => '',
			),
			'hide_admin' => 0,
			'choices' => array(
				'vl1ca' => 'Enspalt (lista under)',
				'vlcards' => 'Cards',
			),
			'allow_null' => 0,
			'default_value' => '2ca',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5c8789cf75141',
			'label' => 'Antal objekt',
			'name' => 'display_quantity',
			'type' => 'number',
			'instructions' => 'Ange antal objekt som skall visas. 0 = alla!',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => 10,
			'placeholder' => '',
			'prepend' => '',
			'append' => 'st. objekt.',
			'min' => 0,
			'max' => '',
			'step' => 1,
		),
		array(
			'key' => 'field_5e4127f0c30c7',
			'label' => 'Begränsa listans höjd?',
			'name' => 'display_max_list_height',
			'type' => 'button_group',
			'instructions' => 'Kryssa i för att begränsa listans höjd och',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'hide_admin' => 0,
			'choices' => array(
				0 => 'Begränsa max-höjden på listan',
				1 => 'Begränsa inte max-höjden på listan',
			),
			'allow_null' => 0,
			'default_value' => 0,
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5ca4af1a50604',
			'label' => 'Inläggstext',
			'name' => 'display_text',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'excerpt' => 'Visa inläggets utdrag',
				'content' => 'Visa inläggets text',
				'none' => 'Visa inte text',
			),
			'allow_null' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5d023f1aa39b4',
			'label' => 'Header',
			'name' => '',
			'type' => 'accordion',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'open' => 0,
			'multi_expand' => 0,
			'endpoint' => 0,
		),
		array(
			'key' => 'field_5d024223a145c',
			'label' => 'Visa header?',
			'name' => 'display_header',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				0 => 'Nej',
				1 => 'Ja',
			),
			'allow_null' => 0,
			'default_value' => 1,
			'layout' => 'horizontal',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5d02456a41d98',
			'label' => '',
			'name' => 'customized',
			'type' => 'group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d024223a145c',
						'operator' => '==',
						'value' => '1',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layout' => 'block',
			'sub_fields' => array(
				array(
					'key' => 'field_5cecdf0785d87',
					'label' => 'Ange egna specifika värden?',
					'name' => 'customized',
					'type' => 'button_group',
					'instructions' => 'Välj om du vill ange egna specifika värden eller ej!',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'choices' => array(
						0 => 'Nej',
						1 => 'Ja',
					),
					'allow_null' => 0,
					'default_value' => 0,
					'layout' => 'horizontal',
					'return_format' => 'value',
				),
				array(
					'key' => 'field_5cecdf0785d88',
					'label' => 'Ange färg',
					'name' => 'customized_color',
					'type' => 'color_picker',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5cecdf0785d87',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '#009FE3',
				),
				array(
					'key' => 'field_5cecdf0785d89',
					'label' => 'Ange rubrik',
					'name' => 'customized_title',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => array(
						array(
							array(
								'field' => 'field_5cecdf0785d87',
								'operator' => '==',
								'value' => '1',
							),
						),
					),
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/kprl-bootstrap-media-list',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5c62a77554027',
	'title' => 'Block: kprl Image Gallery',
	'fields' => array(
		array(
			'key' => 'field_5c62a985d3700',
			'label' => 'Rubrik',
			'name' => 'block_kprl_gallery_title',
			'type' => 'text',
			'instructions' => 'Ange eventuell rubrik. Lämna tomt för att dölja.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => 'Bildgalleri',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5c62a7756c3c4',
			'label' => 'Bildgalleri',
			'name' => 'block_kprl_gallery_gallery',
			'type' => 'gallery',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '100',
				'class' => '',
				'id' => '',
			),
			'min' => 1,
			'max' => '',
			'insert' => 'append',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => 'jpg,jpeg,png,gif',
			'return_format' => 'array',
			'preview_size' => 'medium',
		),
		array(
			'key' => 'field_5c62a7756c452',
			'label' => 'Style',
			'name' => 'block_kprl_gallery_style',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'slideshow' => 'Bildspel',
				'gallery' => 'Galleri',
			),
			'allow_null' => 0,
			'default_value' => 'slide',
			'layout' => 'horizontal',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5c62aa34d3701',
			'label' => 'Antal kolumner',
			'name' => 'block_kprl_gallery_columns',
			'type' => 'number',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5c62a7756c452',
						'operator' => '==',
						'value' => 'gallery',
					),
				),
			),
			'wrapper' => array(
				'width' => '25',
				'class' => '',
				'id' => '',
			),
			'default_value' => 5,
			'placeholder' => '',
			'prepend' => '',
			'append' => 'kolumner',
			'min' => 1,
			'max' => 7,
			'step' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/kprl-image-gallery',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5cbf049e60213',
	'title' => 'Block: kprl Staffing',
	'fields' => array(
		array(
			'key' => 'field_5cbf04caabbaf',
			'label' => 'Layout',
			'name' => 'kprl-staffing-layout',
			'type' => 'button_group',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'plain' => 'Plain, clean and simple',
				'card-horizontal' => 'card-horizontal',
				'card-group' => 'card-group',
				'card-deck' => 'card-deck',
				'card-columns' => 'card-columns',
				'contact-expand' => 'Expandable contact meny',
			),
			'allow_null' => 0,
			'default_value' => '',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5cc0235bccad0',
			'label' => 'Innehåll',
			'name' => 'kprl-staffing-content',
			'type' => 'button_group',
			'instructions' => 'Välj vad som skall visas',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'all-departments' => 'Alla avdelningar',
				'specific-departments' => 'Specifik(a) avdelning(ar)',
				'all-staffing' => 'All bemanning',
				'specific-staffing' => 'Specifik(a) person(er)',
			),
			'allow_null' => 0,
			'default_value' => 'all-departments',
			'layout' => 'vertical',
			'return_format' => 'value',
		),
		array(
			'key' => 'field_5cc030d9e27ac',
			'label' => 'Specifik(a) avdelning(ar)',
			'name' => 'kprl-staffing-specific-departments',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cc0235bccad0',
						'operator' => '==',
						'value' => 'specific-departments',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'kprl_staffing_department',
			'field_type' => 'multi_select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
		array(
			'key' => 'field_5cc029804b783',
			'label' => 'Specifik(a) person(er)',
			'name' => 'kprl-staffing-specific-staffing',
			'type' => 'relationship',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5cc0235bccad0',
						'operator' => '==',
						'value' => 'specific-staffing',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array(
				0 => 'kprl_staffing',
			),
			'taxonomy' => '',
			'filters' => array(
				0 => 'search',
				1 => 'taxonomy',
			),
			'elements' => array(
				0 => 'featured_image',
			),
			'min' => 1,
			'max' => '',
			'return_format' => 'object',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/kprl-staffing',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'permalink',
		1 => 'the_content',
		2 => 'excerpt',
		3 => 'discussion',
		4 => 'comments',
		5 => 'revisions',
		6 => 'slug',
		7 => 'author',
		8 => 'format',
		9 => 'page_attributes',
		10 => 'featured_image',
		11 => 'categories',
		12 => 'tags',
		13 => 'send-trackbacks',
	),
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5cb7325030a44',
	'title' => 'CPT: kprl-staffing',
	'fields' => array(
		array(
			'key' => 'field_5cb7337d9d754',
			'label' => 'Kontaktinformation',
			'name' => 'kontaktinformation',
			'type' => 'flexible_content',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'layouts' => array(
				'layout_5cc6df7113ac8' => array(
					'key' => 'layout_5cc6df7113ac8',
					'name' => 'email',
					'label' => 'Epostadress',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_5cc6df7113ac9',
							'label' => 'Epostadress',
							'name' => 'email',
							'type' => 'email',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cb7358b9aea4' => array(
					'key' => 'layout_5cb7358b9aea4',
					'name' => 'phone',
					'label' => 'Telefonnummer',
					'display' => 'block',
					'sub_fields' => array(
						array(
							'key' => 'field_5cb735b89aea5',
							'label' => 'Telefonnummer',
							'name' => 'phonenumber',
							'type' => 'intl_tel_input',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'separateDialCode' => 0,
							'allowDropdown' => 1,
							'initialCountry' => 'SE',
							'excludeCountries' => '',
							'onlyCountries' => '',
							'preferredCountries' => 'SE',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cb73394a28c2' => array(
					'key' => 'layout_5cb73394a28c2',
					'name' => 'facebook',
					'label' => 'Facebook',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cb733d09d755',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
						),
						array(
							'key' => 'field_5cbdb117d1480',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '/',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cb736b0309ef' => array(
					'key' => 'layout_5cb736b0309ef',
					'name' => 'instagram',
					'label' => 'Instagram',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cb736b0309f0',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
						),
						array(
							'key' => 'field_5cbdb13fd1481',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '@',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cc6f2cb82fec' => array(
					'key' => 'layout_5cc6f2cb82fec',
					'name' => 'twitter',
					'label' => 'Twitter',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cc6f2cb82fed',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => '',
						),
						array(
							'key' => 'field_5cc6f2cb82fee',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '@',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cc6f2eb82fef' => array(
					'key' => 'layout_5cc6f2eb82fef',
					'name' => 'skype',
					'label' => 'Skype',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cc6f2eb82ff0',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => '',
						),
						array(
							'key' => 'field_5cc6f2eb82ff1',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '@',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cc6f2fa82ff2' => array(
					'key' => 'layout_5cc6f2fa82ff2',
					'name' => 'youtube',
					'label' => 'YouTube',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cc6f2fa82ff3',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => '',
						),
						array(
							'key' => 'field_5cc6f2fa82ff4',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '/',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
				'layout_5cc6f31882ff5' => array(
					'key' => 'layout_5cc6f31882ff5',
					'name' => 'github',
					'label' => 'GitHub',
					'display' => 'table',
					'sub_fields' => array(
						array(
							'key' => 'field_5cc6f31882ff6',
							'label' => 'Webbadress',
							'name' => 'link',
							'type' => 'link',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
						),
						array(
							'key' => 'field_5cc6f31882ff7',
							'label' => 'Användarnamn',
							'name' => 'username',
							'type' => 'text',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array(
								'width' => '50',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '/',
							'append' => '',
							'maxlength' => '',
						),
					),
					'min' => '',
					'max' => '',
				),
			),
			'button_label' => 'Lägg till ny kontaktinformation',
			'min' => '',
			'max' => 10,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'kprl_staffing',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => array(
		0 => 'permalink',
		1 => 'excerpt',
		2 => 'discussion',
		3 => 'comments',
		4 => 'revisions',
		5 => 'slug',
		6 => 'author',
		7 => 'format',
		8 => 'page_attributes',
		9 => 'tags',
		10 => 'send-trackbacks',
	),
	'active' => true,
	'description' => '',
));

acf_add_local_field_group(array(
	'key' => 'group_5c93a357aa516',
	'title' => 'kprl Taxonomi-tillägg',
	'fields' => array(
		array(
			'key' => 'field_5c93a3ff4fd5f',
			'label' => 'Färg',
			'name' => 'kwpb-taxonomy-color',
			'type' => 'color_picker',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#009FE3',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'category',
			),
		),
		array(
			array(
				'param' => 'taxonomy',
				'operator' => '==',
				'value' => 'kprl_staffing_department',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;
